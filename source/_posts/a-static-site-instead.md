---
title: A Static Site Instead
date: 2017-02-19 15:07:48
tags:
---
This project is a movement towards an approach to web sites, in a way different from that since [I started in 2005](http://daviding.com/blog/index.php/archive/2005/10/).

It includes:

* A movement [towards static site generators, away from Wordpress](https://poststatus.com/static-site-generators-versus-wordpress/).
* The rise of [distributed content management systems](https://justwriteclick.com/2015/12/17/why-use-github-as-a-content-management-system/) as an alternative to mainstream centralized approaches.
* Automated [continuous integration of source content](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/) from the Markdown source documents into browser-friendly HTML pages.

Wordpress is certainly the most popular alternative amongst web content management systems, but [popularity attracts hackers](https://blogvault.net/the-price-of-popularity-why-hackers-target-wordpress/) .  Alternatives are worth considering.

David revisited Static Site Generators in February 2023.
